/* kunsung5 : Add*/
static void rotate_IFLA_wvec_drE(wilson_vector *src, Real d)
{
  char myname[] = "rotate_IFLA_wvec_drE";
  
  if(src == NULL){
    node0_printf("%s: Error: called with NULL arg\n", myname);
    terminate(1);
  }

  // conversion from MILC to QDP -----------------------------
  create_qopqdp_links(GM,gauge);
  load_wv_to_dirac(qop_src,source,src);
  // ---------------------------------------------------------

  int mu;
  int dir;
  double tad = 1.0/d; // inverse u0
  QLA_Real drEhalf = -tad*0.5*tad*tad*tad*tad;
  QLA_Int aidx[3] = {9,10,12}; /* index of \gamma */
  QLA_Int gidx[4] = {1,2,4,8}; /* index of \gamma */


  //------------
  // allocation
  //------------
  QDP_ColorMatrix *e;
  QDP_DiracFermion *tmpD1, *tmpD2, *alpEpsi;
 
  e = QDP_create_M();

  tmpD1 = QDP_create_D();
  tmpD2 = QDP_create_D();
  alpEpsi = QDP_create_D();
  
  QDP_DiracFermion *psi_fwd[3], *psi_bac[3], *psi_minus[3];
  
  for (dir=0; dir<3; dir++) {
    psi_fwd[dir]   = QDP_create_D();
    psi_bac[dir]   = QDP_create_D();
    psi_minus[dir] = QDP_create_D();
  }

  QDP_DiracFermion *source_tmp;
  source_tmp = QDP_create_D();  // tmp source copy
  QDP_D_eq_D(source_tmp,source,QDP_all);


  //----
  // dE
  //----
  QDP_D_eq_zero(alpEpsi,QDP_all);
  for(dir=0;dir<3;dir++){
    f_mu_nu_private(e,1.0,gauge,3,dir);
    QDP_D_eq_M_times_D(tmpD1,e,source_tmp,QDP_all);
    QDP_D_eq_gamma_times_D(tmpD2,tmpD1,aidx[dir],QDP_all); // chech alpha sign
    QDP_D_peq_D(alpEpsi,tmpD2,QDP_all);
  }

  //----
  // d1
  //----
  for (dir=0; dir<3; dir++) { 
    QDP_D_eq_M_times_sD(psi_fwd[dir],
                        gauge[dir],
                        alpEpsi,QDP_neighbor[dir],QDP_forward,QDP_all);
    
    QDP_D_eq_Ma_times_D(tmpD1,gauge[dir],alpEpsi,QDP_all);
    QDP_D_eq_sD(psi_bac[dir],
                tmpD1,QDP_neighbor[dir],QDP_backward,QDP_all);

    QDP_D_eq_D_minus_D(tmpD1,psi_fwd[dir],psi_bac[dir],QDP_all);
    QDP_D_eq_gamma_times_D(psi_minus[dir],tmpD1,gidx[dir],QDP_all);
  }
  QDP_D_eq_D(source_tmp,psi_minus[0],QDP_all);
  QDP_D_peq_D(source_tmp,psi_minus[1],QDP_all);
  QDP_D_peq_D(source_tmp,psi_minus[2],QDP_all);
  
  //---------------------------------------------------------

  //----
  // d1
  //----
  for (dir=0; dir<3; dir++) { 
    QDP_D_eq_M_times_sD(psi_fwd[dir],
                        gauge[dir],
                        source,QDP_neighbor[dir],QDP_forward,QDP_all);
    
    QDP_D_eq_Ma_times_D(tmpD1,gauge[dir],source,QDP_all);
    QDP_D_eq_sD(psi_bac[dir],
                tmpD1,QDP_neighbor[dir],QDP_backward,QDP_all);

    QDP_D_eq_D_minus_D(tmpD1,psi_fwd[dir],psi_bac[dir],QDP_all);
    QDP_D_eq_gamma_times_D(psi_minus[dir],tmpD1,gidx[dir],QDP_all);
  }
  QDP_D_eq_D(source,psi_minus[0],QDP_all);
  QDP_D_peq_D(source,psi_minus[1],QDP_all);
  QDP_D_peq_D(source,psi_minus[2],QDP_all);
  
  //----
  // dE
  //----
  QDP_D_eq_zero(alpEpsi,QDP_all);
  for(dir=0;dir<3;dir++){
    f_mu_nu_private(e,1.0,gauge,3,dir);
    QDP_D_eq_M_times_D(tmpD1,e,source,QDP_all);
    QDP_D_eq_gamma_times_D(tmpD2,tmpD1,aidx[dir],QDP_all); // chech alpha sign
    QDP_D_peq_D(alpEpsi,tmpD2,QDP_all);
  }

  //----------------------------------------------------------
  // source_tmp += alpEpsi
  QDP_D_peq_D(source_tmp,alpEpsi,QDP_all);
  QDP_D_eq_r_times_D(source,&drEhalf,source_tmp,QDP_all);

  // conversion from QDP to MILC -----------------------------
  unload_dirac_to_wv(qop_src,source,src);
  destroy_qopqdp_links(&GM);
  // ---------------------------------------------------------
  
  QDP_destroy_M(e);

  QDP_destroy_D(tmpD1);
  QDP_destroy_D(tmpD2);
  QDP_destroy_D(alpEpsi);
  QDP_destroy_D(source_tmp);
  


} /* rotate_IFLA_wvec_drE */
